package_name=genefistub
PREFIX?=/usr

clean:
	rm $(package_name).8.gz

$(package_name).8.gz: $(package_name).8
	@if test -f $(package_name).8.gz; then\
		rm $(package_name).8.gz;\
	fi
	gzip -c $(package_name).8 > $(package_name).8.gz

install: $(package_name) $(package_name).8.gz
	install -Dm755 $(package_name) $(PREFIX)/bin/$(package_name)
	install -Dm644 $(package_name).8.gz $(PREFIX)/share/man/man8/$(package_name).8.gz
