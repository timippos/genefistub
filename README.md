genefistub
==========

A simple tool to generate EFI boot entry for Arch Linux using LVM on LUKS.

This is meant for my private use, and it is very likely to break your system if you are not sure what you are doing. Use with extreme caution!